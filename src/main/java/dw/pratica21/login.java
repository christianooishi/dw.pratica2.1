package dw.pratica21;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Universidade Tecnológica Federal do Paraná
 * IF6AE Desenvolvimento de Aplicaçoões Web
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
@WebServlet(name = "Autenticação", urlPatterns = {"/login"})
public class login extends HttpServlet {

    /**
     * Processa requisições HTTP para os métodos
     * <code>GET</code> e
     * <code>POST</code>.
     *
     * @param request Requisição
     * @param response Resposta
     * @throws ServletException Se ocorrer um exceção específica de Servlet
     * @throws IOException Se ocorrer uma exceção de E/S
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String login = request.getParameter("login");
            String senha = request.getParameter("senha");
            String perfil = request.getParameter("perfil");
            if(login.equals("cliente") && senha.equals("cliente") && perfil.equals("1")){
                out.println("<script>location.href='sucesso?login="+login+"&perfil="+perfil+"'</script>");
            }
            else if(login.equals("gerente") && senha.equals("gerente") && perfil.equals("2")){
                out.println("<script>location.href='sucesso?login="+login+"&perfil="+perfil+"'</script>");
            }
            else if(login.equals("admin") && senha.equals("admin") && perfil.equals("3")){
                out.println("<script>location.href='sucesso?login="+login+"&perfil="+perfil+"'</script>");
            }
            else{
                out.println("<script>location.href='erro.xhtml'</script>");
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Processa a requisição HTTP para o método
     * <code>GET</code>.
     *
     * @param request Requisição
     * @param response Resposta
     * @throws ServletException Se ocorrer um exceção específica de Servlet
     * @throws IOException Se ocorrer uma exceção de E/S
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Processa a requisição HTTP para o método
     * <code>POST</code>.
     *
     * @param request Requisição
     * @param response Resposta
     * @throws ServletException Se ocorrer um exceção específica de Servlet
     * @throws IOException Se ocorrer uma exceção de E/S
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Retorna uma descrição resumida do servlet.
     * @return Uma descrição resumida.
     */
    @Override
    public String getServletInfo() {
        return "Exemplo de servlet simples";
    }// </editor-fold>
}
